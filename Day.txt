public enum Day {
	MONDAY(Calendar.MONDAY),
	TUESDAY(Calendar.TUESDAY),
	WEDNESDDAY(Calender.WEDNESDAY),
	THURSDAY(Calendar.THURSDAY),
	FRIDAY(Calendar.FRIDAY),
	SATURDAY(Calendar.SATURDAY),
	SUNDAY(Calendar.SUNDAY);
	
	public final int index;
	private static DateFormatSymbols dateSymbols=new DateFormatSymbols();
	
	Day(int day){
		index=day;
	}
	
	public static Day make(int index) throws IllegalArgumentException {
		for(Day d:Day.values())
			if (d.index==index)
				return d;
		throw new IllegalAccessException(
			String.format("Illegal day index:%d.",index));
	}
	
	public static Day parse(String s) throws IllegalArgumentException{
		String[] shortWeekdayNames=dateSymbols.getShortWeekdays();
		String[] weekDayNames=dateSymbols.getWeekdays();
		
		s=s.trim();
		for(Day day:Day.values()){
			if(s.equalsIgnoreCase(shortWeekdayNames[day.index]) ||
			s.equalsIgnoreCase(weekDayNames[day.index])){
				return day;
			}
		}
		throws new IllegalAccessException(
		String.format("%is not a valid weekday string",s));
	}
	
	public String toString(){
		return dateSymbols.getWeekdays()[index];
	}
}