package junit.tests.framework;

import junit.framework.ComparisonCompactor;
import junit.framework.TestCase;

public class ComparisonCompactorTest extends TestCase{
	public void testMessage(){
		String failure=new ComparisonCompactor(0,"b","c").compact("a");
		assertTrue("a expected:<[b]> but was:<[c]>".equals(failure));
	}
	
	public void testStartSame(){
		String failure=new ComparisonCompactor(1,"ba","bc").compact(null);
		assertEquals("expected:<b[a]> but was:<b[c]>",failure);
	}
	
	public void testEndSame(){
		String failure=new ComparisonCompactor(1,"ab","cb").compact(null);
		assertEquals("expected:<[a]b> but was:<[c]b>",failure);
	}
	
	public void testSame(){
		String failure=new ComparisonCompactor(1,"ab","ab").compact(null);
		assertEquals("expected:<ab> but was:<ab>",failure);
	}
	
	public void testNoContextStartAndEndSame(){
		String failure=new ComparisonCompactor(0,"abc","adc").compact(null);
		assertEquals("expected:<...[b]...> but was:<...[d]...>",failure);
	}
	
	public void testStartAndEndContext(){
		String failure=new ComparisonCompactor(1,"abc","adc").compact(null);
		assertEquals("expected:<a[b]c> but was:<a[d]c>",failure);
	}
	
	public void testStartAndEndContextWithEllipses(){
		String failure=new ComparisonCompactor(1,"abcd","abfde").compact(null);
		assertEquals("expected:<...b[c]d...> but was:<...b[f]d>",failure);
	}
	
	public void testComparisionErrorStartSameComplete(){
		String failure=new ComparisonCompactor(2,"ab","abc").compact(null);
		assertEquals("expected:<ab[]> but was:<ab[c]>",failure);
	}
	
	public void testComparisonErrorEndSameComplete(){
		String failure=new ComparisonCompactor(0,"bc","abc").compact(null);
		assertEquals("expected:<[]...> but was:<[a]...>",failure);
	}
	
	public void testComparisonErrorEndSameCompleteContext(){
		String failure=new ComparisonCompactor(2,"bc","abc").compact(null);
		assertEquals("expected:<[]bc> but was:<[a]bc>",failure);
	}
	
	public void testComparisionErrorOverlapingMatches(){
		String failure=new ComparisonCompactor(0,"abc","abbc").compact(null);
		assertEquals("expected:<...[]...> but was:<...[b]...>",failure);
	}
	
	public void testComparisionErrorOverlapingMatchesContext(){
		String failure=new ComparisonCompactor(2,"abc","abbc").compact(null);
		assertEquals("expected:<ab[]c> but was:<ab[b]c>",failure);
	}
	
	public void testComparisionErrorOverlapingMatches2(){
		String failure=new ComparisonCompactor(0,"abcdde","abcde").compact(null);
		assertEquals("expected:<...[d]...> but was:<...[]...>",failure);
	}
	
	public void testComparisionErrorOverlapingMatches2Context(){
		String failure=new ComparisonCompactor(2,"abcdde","abcde").compact(null);
		assertEquals("expected:<...cd[d]e> but was:<...cd[]e>",failure);
	}
	
	public void testComparisionErrorWithActualNull(){
		String failure=new ComparisonCompactor(0,"a",null).compact(null);
		assertEquals("expected:<a> but was:<null>",failure);
	}
	
	public void testComparisonErrorWithActualNullContext(){
		String failure=new ComparisonCompactor(2,"a",null).compact(null);
		assertEquals("expected:<a> but was:<null>",failure);
	}
	
	public void testComparisionErrorWithExpectedNull(){
		String failure=new ComparisonCompactor(0,null,"a").compact(null);
		assertEquals("expected:<null> but was:<a>",failure);
	}
	
	public void testComparisionErrorWithExpectedNullContext(){
		String failure=new ComparisonCompactor(2,null,"a").compact(null);
		assertEquals("expected:<null> but was:<a>",failure);
	}
	
	public void testBug609972(){
		String failure=new ComparisonCompactor(10,"S&P500","0").compact(null);
		assertEquals("expected:<[S&P50]0> but was:<[]0>",failure);
	}
	
}

//代碼被100%覆蓋了。每行代碼、每個if語句和for循環都被測試執行了。