package junit.framework;

public class ComparisonCompactor{
	
	private static final String ELLIPSIS="...";
	private static final String DELTA_END="]";
	private static final String DELTA_START="[";
	
	private int contextLength;
	private String expected;
	private String actual;
	private int prefixLength;
	private int suffixLength;
	
	public ComparisonCompactor(int contextLength,String expected,String actual){
		this.contextLength=contextLength;
		this.expected=expected;
		this.actual=actual;
	}
	
	public String formatCompactedComparison(String message){
		private String compactExpected=expected;
		private String compactActual=actual;
		if(shouldBeCompacted()){
			findCommonPrefixAndSuffix();
			compactExpected=compactString(expected);
			compactActual=compactString(actual);
		} 
		return Assert.format(message,compactExpected,compactActual);
		
		
	}
	private void compactExpectedAndActual(){
		findCommonPrefixAndSuffix();
		compactExpected=compactString(expected);
		compactActual=compactString(actual);
	}

	private void shouldBeCompacted(){
		return !shouldNotBeCompacted();
	}
	
	
	private void shouldNotBeCompacted(){
		return expected==null || actual ==null || areStringsEqual();
	}
	
	private void findCommonPrefixAndSuffix(){
		findCommonPrefix();
		suffixLength=0;
		for (; !suffixOverlapsPrefix(suffixLength);suffixLength++){
			if(charFromEnd(expected,suffixLength) != charFromEnd(actual,suffixLength))
				break;
		}
	}
	
	private char charFromEnd(String s,int i){
		return s.charAt(s.length()-i-1);
	}
	
	private boolean suffixOverlapsPrefix(int suffixLength){
		return actual.length() -suffixLength<=prefixLength || expected.length()-suffixLength<=prefixLength;
	}
	
	
	private int findCommonPrefix(){
		int prefixIndex=0;
		int end=Math.min(expected.length(),actual.length());
		for(;prefixIndex<end;prefixIndex++){
			if(expected.charAt(prefixIndex)!=actual.charAt(prefixIndex))
				break;
		}
		return prefixIndex;
	}
	
	private String compact(String s){
		return new StringBuilder()
		.append(startingEllipsis())
		.append(startingContext())
		.append(DELTA_START)
		.append(delta(s))
		.append(DELTA_END)
		.append(endingContext())
		.append(endingEllipsis())
		.toString();
	}
	
	private String startingEllipsis(){
		return prefixLength>contextLength?ELLIPSIS:"";
	}
	
	private String startingContext(){
		int contextStart=Math.max(0,prefixLength-contextLength);
		int contextEnd=prefixLength;
		return expected.substring(contextStart,contextEnd);
	}
	
	private String delta(String s){
		int deltaStart=prefixLength;
		int deltaEnd=s.length()-suffixLength;
		return s.substring(deltaStart,deltaEnd);
	}
	
	private String endingContext(){
		int contextStart=expected.length()-suffixLength;
		int contextEnd=Math.min(contextStart+contextLength,expected.length());
		return expected.substring(contextStart,contextEnd);
	}
	
	private String endingEllipsis(){
		return (suffixLength>contextLength?ELLIPSIS:"");
	}
}